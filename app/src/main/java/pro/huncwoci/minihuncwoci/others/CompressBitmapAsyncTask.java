package pro.huncwoci.minihuncwoci.others;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;

/**
 * Created by su on 13.01.18.
 */

public class CompressBitmapAsyncTask extends AsyncTask<Void, Void, byte[]> {

    public interface ICompressBitmapCallback {
        void bitmapDidCompress(byte[] bitmapByteArray);
    }

    private Bitmap bitmap;
    private ICompressBitmapCallback callback;

    public CompressBitmapAsyncTask(Bitmap bitmap, ICompressBitmapCallback callback) {
        this.bitmap = bitmap;
        this.callback = callback;
    }

    @Override
    protected void onPostExecute(byte[] byteArray) {
        this.callback.bitmapDidCompress(byteArray);
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected byte[] doInBackground(Void... params) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }

}
