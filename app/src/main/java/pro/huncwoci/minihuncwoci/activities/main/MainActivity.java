package pro.huncwoci.minihuncwoci.activities.main;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.indoorway.android.fragments.sdk.map.IndoorwayMapFragment;
import com.indoorway.android.fragments.sdk.map.MapFragment;

import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.activities.BaseActivity;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;

public class MainActivity extends BaseActivity implements IndoorwayMapFragment.OnMapFragmentReadyListener{

    private final int ACCESS_FINE_LOCATION_PERMISSION = 2222;

    private IMainActivityViewCallback viewCallback;
    private IMainActivityPresenterCallback presenter;

    private boolean isPermsGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        setStatusBarGradiant(this);
        SessionManager.getInstance(MainActivity.this);
        View v = getWindow().getDecorView();

        //init mvp
        this.presenter = new MainActivityPresenter();
        this.viewCallback = new MainActivityViewController(this, v, getLayoutInflater());
        this.presenter.setViewCallback(this.viewCallback);
        this.presenter.viewLoaded();

        //check dangerous permissions and start map
        if(!isPermsGranted){
            checkDangerousPermissions();
        }else{
            createMapFragment();
        }

    }

    // Set transparent status bar for AppCompatActivity
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }


    private void createMapFragment() {
        IndoorwayMapFragment.Config config = new IndoorwayMapFragment.Config();
        // is floating location button visible?
        config.setLocationButtonVisible(true);
        // do you want to start location on each fragment resume?
        config.setStartPositioningOnResume(true);
        // load last known map based on user location?
        config.setLoadLastKnownMap(true);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        IndoorwayMapFragment mapFragment = IndoorwayMapFragment.newInstance(this, config);
        fragmentTransaction.add(R.id.main_activityFragmentContainer, mapFragment, IndoorwayMapFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void onMapFragmentReady(MapFragment mapFragment) {
        viewCallback.setMapFragment(mapFragment);
    }

    //PERMISSIONS
    private void checkDangerousPermissions() {
        boolean isLocationPermissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if(!isLocationPermissionGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION);
                return;
            }
        }

        if(isLocationPermissionGranted) {
            this.isPermsGranted = true;
            createMapFragment();
            presenter.permissionsGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if(requestCode == ACCESS_FINE_LOCATION_PERMISSION) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkDangerousPermissions();
            }else {
                Toast.makeText(this, "The app was not allowed to access fine location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
