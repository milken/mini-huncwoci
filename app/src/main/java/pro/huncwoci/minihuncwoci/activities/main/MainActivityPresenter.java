package pro.huncwoci.minihuncwoci.activities.main;

import android.util.Log;

import com.indoorway.android.common.sdk.IndoorwaySdk;
import com.indoorway.android.common.sdk.listeners.generic.Action1;
import com.indoorway.android.common.sdk.model.Coordinates;
import com.indoorway.android.common.sdk.model.IndoorwayPosition;
import com.indoorway.android.common.sdk.model.RegisteredVisitor;
import com.indoorway.android.common.sdk.model.Visitor;
import com.indoorway.android.common.sdk.model.VisitorLocation;
import com.indoorway.android.common.sdk.task.IndoorwayTask;
import com.indoorway.android.map.sdk.view.MapView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pro.huncwoci.minihuncwoci.callbacks.IDataSourceCallback;
import pro.huncwoci.minihuncwoci.models.base.ActionManager;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;
import pro.huncwoci.minihuncwoci.models.base.UserActionManager;
import pro.huncwoci.minihuncwoci.models.next_lesson.NextLessonDataSource;
import pro.huncwoci.minihuncwoci.models.next_lesson.NextLessonEntity;
import pro.huncwoci.minihuncwoci.models.pois.PoiEntity;
import pro.huncwoci.minihuncwoci.models.pois.PoisDataSource;
import pro.huncwoci.minihuncwoci.models.pois.PoisEntity;

/**
 * Created by su on 13.01.18.
 */

public class MainActivityPresenter implements IMainActivityPresenterCallback {

    private static final String TAG = "presenterTag";
    private IMainActivityViewCallback viewCallback;

    private NextLessonDataSource nextLessonDataSource;
    private PoisDataSource poisDataSource;

    private int selectedModule = 0;

    private boolean isMapConfigured = false;
    private boolean isTabLayoutConfigured = false;
    private boolean isPositionObtained = false;

    private ArrayList<RegisteredVisitor> visitors;

    //init presenter
    public MainActivityPresenter() {
        fetchPois();
    }

    private void fetchPois() {
        this.poisDataSource = new PoisDataSource();
        this.poisDataSource.fetchData(new IDataSourceCallback() {
            @Override
            public void fetchCompleted(boolean success, int errorCode, int itemsCount) {
                Log.d(TAG, "fetchCompleted: poisDataSource = "+success);
                if(success){
                    checkVisitor();
                    getVisitors();
                }else{
                    viewCallback.showNetworkErrorDialog();
                }
            }
        });
    }

    // visitor methods
    private void checkVisitor() {
        if(!SessionManager.isVisitorSet() || ((PoisEntity)poisDataSource.getItem()).getName().equals(IndoorwaySdk.instance().visitor().me().getName())){
            createVisitor();
        }
    }

    private void createVisitor(){
        Visitor visitor = new Visitor();
        // optional: set more detailed informations if you have one
        visitor.setGroupUuid("Huncwoci");               // user group
        visitor.setName(((PoisEntity)poisDataSource.getItem()).getName());       // user name
        visitor.setShareLocation(true);

        IndoorwaySdk.instance().visitor().setup(visitor);
        SessionManager.visitorSet();
    }

    private void getVisitors() {
        this.visitors = new ArrayList<>();
        IndoorwaySdk.instance()
                .visitors()
                .list()
                .setOnCompletedListener(new Action1<List<RegisteredVisitor>>() {
                    @Override
                    public void onAction(List<RegisteredVisitor> registeredVisitors) {
                        for(RegisteredVisitor registeredVisitor: registeredVisitors){
                            Log.d(TAG, "onAction: reg visitor = "+registeredVisitor.getUuid());
                            visitors.add(registeredVisitor);
                        }
                    }
                })
                .setOnFailedListener(new Action1<IndoorwayTask.ProcessingException>() {
                    @Override
                    public void onAction(IndoorwayTask.ProcessingException e) {
                        // show error toast

                    }
                }).execute();
    }

    // lifecycle like methods
    @Override
    public void setViewCallback(IMainActivityViewCallback viewCallback) {
        this.viewCallback = viewCallback;
    }

    @Override
    public void viewLoaded() {
        this.viewCallback.setupView(this);
    }

    @Override
    public void viewSetUp() {
        this.viewCallback.configView();
    }

    @Override
    public void onPause() {
        viewCallback.onPause();
    }

    @Override
    public void permissionsGranted() {
        //doesnt matter!
    }

    // modules load callbacks
    @Override
    public void tabLayoutConfigured() {
        if(!isTabLayoutConfigured){
            this.isTabLayoutConfigured = true;
            updateSelectedModule();
        }

    }

    @Override
    public void mapLoaded() {
        this.isMapConfigured = true;
        updateSelectedModule();


    }

    @Override
    public void locationObtained() {
        if(!isPositionObtained){
            this.isPositionObtained = true;
            updateSelectedModule();
        }
    }

    // module change
    @Override
    public void tabSelected(int position) {
        this.selectedModule = position;
        updateSelectedModule();

    }

    // save coord - normally disabled
    @Override
    public void saveLocationClicked(String desc, IndoorwayPosition indoorwayPosition) {
        UserActionManager.postPoi(desc, indoorwayPosition.getCoordinates(), indoorwayPosition.getMapUuid(), new ActionManager.IActionCallback() {
            @Override
            public void completed(boolean success, int errorCode, JSONObject response) {
                viewCallback.showSuccessToast();
            }
        });
    }

    // rois module
    @Override
    public void searchClicked() {
        ArrayList<String> types = new ArrayList<>();
        for(PoiEntity poiEntity: ((PoisEntity) poisDataSource.getItem()).getPoiList()){
            String type = poiEntity.getType();
            if(!type.equals("room") && !type.isEmpty())
                types.add(type);
        }

        Set<String> uniqueTypes = new HashSet<>(types);
        types.clear();
        types.addAll(uniqueTypes);

        viewCallback.showTypeChooseDialog(types);
    }

    @Override
    public void typeChosen(String type) {
        Log.d(TAG, "typeChosen: type = "+type);
        ArrayList<PoiEntity> entities = new ArrayList<>();
        for(PoiEntity poiEntity: ((PoisEntity) poisDataSource.getItem()).getPoiList()){
            if(poiEntity.getType().equals(type)){
                entities.add(poiEntity);
            }
        }

        MapView.NavigationController navigationController = viewCallback.getNavigationController();

        double minDistance = Float.MAX_VALUE;
        PoiEntity resultEntity = null;
        for(PoiEntity poiEntity: entities){
            double distance = navigationController.getDistance(poiEntity.getCoordinates());
                if(distance<minDistance){
                    minDistance = distance;
                    resultEntity = poiEntity;
                }
            }

        viewCallback.startNavigation(resultEntity.getCoordinates());
    }

    // get visitor for id from visitorLocation - not current user!
    @Override
    public RegisteredVisitor findVisitor(String visitorUuid) {
        for(RegisteredVisitor visitor: visitors){
            if(visitor.getUuid().equals(visitorUuid) && !visitor.getName().equals(IndoorwaySdk.instance().visitor().me().getName())){
                return visitor;
            }
        }
        return null;
    }

    // private modules init methods
    private void updateSelectedModule() {
        if(isMapConfigured && isTabLayoutConfigured && isPositionObtained) {
            switch (selectedModule){
                case 0:
                    initNextLesson();
                    break;
                case 1:
                    initRois();
                    break;
                case 2:
                    initFriends();
//                    initSave();
                    break;
            }
        }

    }

    private void initFriends() {
        Calendar calendar = Calendar.getInstance();
        final long currentTime = calendar.getTimeInMillis();



        Log.d(TAG, "initFriends: currentTime = "+currentTime);
        IndoorwaySdk.instance()
                .visitors()
                .locations()
                .setOnCompletedListener(new Action1<List<VisitorLocation>>() {
                    @Override
                    public void onAction(List<VisitorLocation> visitorLocations) {
                        ArrayList<VisitorLocation> visitors = new ArrayList<>();

                        Log.d(TAG, "onAction: gut");
                        for(VisitorLocation visitorLocation: visitorLocations){
                            if(visitorLocation.getPosition()!=null && visitorLocation.getTimestamp()!=null){
                                if(Math.abs(currentTime-visitorLocation.getTimestamp().getTime())<60*1000){
                                    visitors.add(visitorLocation);
                                }
                            }
                        }

                        viewCallback.showVisitors(visitors);
                    }
                })
                .setOnFailedListener(new Action1<IndoorwayTask.ProcessingException>() {
                    @Override
                    public void onAction(IndoorwayTask.ProcessingException e) {
                        Log.d(TAG, "onAction: error");
                    }
                })
                .execute();

    }

    private void initRois() {
        viewCallback.showRois(((PoisEntity) poisDataSource.getItem()).getPoiList());

    }

    private void initSave() {
        this.viewCallback.showSaveLayout();
    }

    private void initNextLesson() {
        Log.d(TAG, "initNextLesson: ");
        if(nextLessonDataSource!=null){
            nextLessonDataSource.cancel();
        }
        this.nextLessonDataSource = new NextLessonDataSource();
        this.nextLessonDataSource.fetchData(new IDataSourceCallback() {
            @Override
            public void fetchCompleted(boolean success, int errorCode, int itemsCount) {
                Log.d(TAG, "fetchCompleted: nextLessonDataSource success = "+success);
                if(success){
                    NextLessonEntity entity = (NextLessonEntity) nextLessonDataSource.getItem();
                    Log.d(TAG, "fetchCompleted: entity name = "+entity.getName());
                    viewCallback.showNextClassInfo(entity);
                    prepareNavigation(entity.getRoom());
                }else{
                    if(errorCode==404){
                        viewCallback.showGoHome();
                    }else{
                        viewCallback.showNetworkErrorDialog();
                    }
                }
            }
        });
    }

    // prepare navigation
    private void prepareNavigation(int roomId) {
        Log.d(TAG, "prepareNavigation: prepareNavigation = "+roomId);
        for(PoiEntity poiEntity: ((PoisEntity) poisDataSource.getItem()).getPoiList()){
            Log.d(TAG, "prepareNavigation: poi id = "+poiEntity.getId());
            if(poiEntity.getId() == roomId){
                Log.d(TAG, "prepareNavigation: "+poiEntity.getName());
                viewCallback.startNavigation(new Coordinates(poiEntity.getLat(), poiEntity.getLng()));
            }
        }
    }
}
