package pro.huncwoci.minihuncwoci.activities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.callbacks.IFragmentCallback;
import pro.huncwoci.minihuncwoci.callbacks.ILoadingDialogCallback;
import pro.huncwoci.minihuncwoci.views.dialogs.LoadingDialog;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity implements IFragmentCallback, ILoadingDialogCallback {

    private LoadingDialog loadingDialog;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStatusBarColor();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    //
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    //////// LOADING
    @Override
    public void showLoadingDialog(boolean isTransparent) {
        this.loadingDialog = new LoadingDialog(BaseActivity.this, isTransparent);
        this.loadingDialog.show();
    }

    @Override
    public void hideLoadingDialog() {
        if(this.loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }


    //////// STATUS BAR
    private void setStatusBarColor() {
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.md_black_1000));
        }

    }

    //DataFragmentCallback

    @Override
    public void showWindow(Fragment fragment, boolean isBack) {

    }

    @Override
    public void showWindow(Fragment fragment, boolean isBack, Fragment targetFragment, int requestCode) {

    }

    @Override
    public void showToolbar() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().show();
        }
    }

    @Override
    public void hideToolbar() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    @Override
    public void showArrowBack() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void hideArrowBack() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void setTitle(String title) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void backWindow() {

    }

    @Override
    public void backWindowWithResult(Fragment currentFragment, Intent data) {

    }
}
