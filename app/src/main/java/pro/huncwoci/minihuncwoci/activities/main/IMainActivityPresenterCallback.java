package pro.huncwoci.minihuncwoci.activities.main;

import com.indoorway.android.common.sdk.model.IndoorwayPosition;
import com.indoorway.android.common.sdk.model.RegisteredVisitor;

/**
 * Created by su on 13.01.18.
 */


public interface IMainActivityPresenterCallback {

    // lifecycle like methods
    void setViewCallback(IMainActivityViewCallback viewCallback);
    void viewLoaded();
    void viewSetUp();
    void onPause();
    void permissionsGranted();

    // modules load
    void tabLayoutConfigured();
    void mapLoaded();
    void locationObtained();

    // module change
    void tabSelected(int position);

    // save coord - normally disabled
    void saveLocationClicked(String desc, IndoorwayPosition indoorwayPosition);

    // rois module
    void searchClicked();
    void typeChosen(String type);

    // find visitor data - friends module
    RegisteredVisitor findVisitor(String visitorUuid);

}