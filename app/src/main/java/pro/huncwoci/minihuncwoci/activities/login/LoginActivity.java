package pro.huncwoci.minihuncwoci.activities.login;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.activities.main.MainActivity;
import pro.huncwoci.minihuncwoci.models.base.ActionManager;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;
import pro.huncwoci.minihuncwoci.models.login.LoginActionManager;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_activityContainer)
    LinearLayout container;

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        setStatusBarGradiant(this);

        SessionManager.getInstance(this);

        //generate random id as hash
        if(!SessionManager.hasUniqueKey()){
            String uniqueID = UUID.randomUUID().toString();
            SessionManager.setUniqueKey(uniqueID);
        }

        this.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLogin();
            }
        });
    }

    // hide status bar
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }


    private void getLogin() {
        this.webView = new WebView(this);
        container.addView(webView, 0);
        LoginActionManager.logIn(new ActionManager.IActionCallback() {
            @Override
            public void completed(boolean success, int errorCode, JSONObject response) {
                Log.d("myTag", "completed: response = "+response);
                // if success then start map
                if(success){
                    Log.d("myTag", "completed: success");
                    //fetch Data
                    startMap();
                    return;

                }else{
                    // otherwise get url to connect with USOS
                    try {
                        if(response!=null) {
                            String url = response.getString("url");
                            configWebView(url);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void startMap() {
        // hide webview
        this.webView.setVisibility(View.GONE);
        // make THAT COOL TRANSITION ANIMATION - 5s time
        TransitionDrawable transition = (TransitionDrawable) container.getBackground();
        transition.startTransition(5000);
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            }
        },7500);
    }

    private void configWebView(String url) {
        webView.loadUrl(url);
        // doesnt matter
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onCloseWindow(WebView window) {
                super.onCloseWindow(window);

                Log.d("loginTag", "onCloseWindow: ");
            }
        });

        // when user is moved to our server, finish actions with webview and start map
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d("loginTag", "onPageFinished: "+url);
                if(url.startsWith("http://138.68.66.216:10000")){
                    Log.d("loginTag", "onPageFinished: should end");
                    startMap();

                }
            }
        });
    }
}
