package pro.huncwoci.minihuncwoci.activities.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indoorway.android.common.sdk.listeners.generic.Action1;
import com.indoorway.android.common.sdk.model.Coordinates;
import com.indoorway.android.common.sdk.model.IndoorwayMap;
import com.indoorway.android.common.sdk.model.IndoorwayNode;
import com.indoorway.android.common.sdk.model.IndoorwayObjectParameters;
import com.indoorway.android.common.sdk.model.IndoorwayPosition;
import com.indoorway.android.common.sdk.model.RegisteredVisitor;
import com.indoorway.android.common.sdk.model.VisitorLocation;
import com.indoorway.android.common.sdk.model.proximity.IndoorwayNotificationInfo;
import com.indoorway.android.common.sdk.model.proximity.IndoorwayProximityEvent;
import com.indoorway.android.common.sdk.model.proximity.IndoorwayProximityEventShape;
import com.indoorway.android.fragments.sdk.map.MapFragment;
import com.indoorway.android.location.sdk.IndoorwayLocationSdk;
import com.indoorway.android.location.sdk.model.IndoorwayLocationSdkState;
import com.indoorway.android.map.sdk.view.MapView;
import com.indoorway.android.map.sdk.view.camera.ScaleFactorType;
import com.indoorway.android.map.sdk.view.drawable.figures.DrawableIcon;
import com.indoorway.android.map.sdk.view.drawable.figures.DrawableText;
import com.indoorway.android.map.sdk.view.drawable.layers.MarkersLayer;
import com.indoorway.android.map.sdk.view.drawable.textures.BitmapTexture;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.models.next_lesson.NextLessonEntity;
import pro.huncwoci.minihuncwoci.models.pois.PoiEntity;
import pro.huncwoci.minihuncwoci.views.base.FontTextView;
import pro.huncwoci.minihuncwoci.views.dialogs.ChooseTypeDialog;

/**
 * Created by su on 13.01.18.
 */

public class MainActivityViewController implements IMainActivityViewCallback{

    private static final String TAG = "myTag";

    //BIND VIEWS
    @BindView(R.id.main_activityTabLayout)
    TabLayout tabLayout;
    @BindView(R.id.main_activityFragmentContainer) FrameLayout fragmentContainer;

    @BindView(R.id.main_activityDetailsContainer) LinearLayout detailsContainer;
    @BindView(R.id.main_activityFirstDetailsContainer) LinearLayout firstDetailsContainer;
    @BindView(R.id.main_activityFirstDetails) TextView firstDetailsTextView;

    @BindView(R.id.main_activitySaveContainer) LinearLayout saveContainer;
    @BindView(R.id.main_activitySaveButton) Button saveButton;
    @BindView(R.id.main_activitySaveEditText) EditText saveEditText;

    @BindView(R.id.main_activitySearchButton) Button searchButton;

    // BIND STRINGS
    @BindString(R.string.mini_building_id) String miniId;
    @BindString(R.string.mini_floor0_id) String floor0Id;
    @BindString(R.string.mini_floor1_id) String floor1Id;
    @BindString(R.string.mini_floor2_id) String floor2Id;

    @BindString(R.string.break_texture_id) String breakTextureName;
    @BindString(R.string.dean_texture_id) String deanTextureName;
    @BindString(R.string.feet_texture_id) String feetTextureName;
    @BindString(R.string.printer_texture_id) String printerTextureName;
    @BindString(R.string.toilet_texture_id) String toiletTextureName;

    //
    private Context context;
    private final LayoutInflater inflater;
    private IMainActivityPresenterCallback presenter;

    private MapFragment mapFragment;

    private Action1<IndoorwayProximityEvent> eventsListener;
    private Action1<IndoorwayLocationSdkState> initialLocationListener;
    private Action1<IndoorwayLocationSdkState> navigationLocationListener;

    private MarkersLayer markersLayer;
    private int visitorCounter = 0;
    private int poiConuter = 0;

    public MainActivityViewController(Context context, View v, LayoutInflater layoutInflater) {
        this.context = context;
        ButterKnife.bind(this, v);
        this.inflater = layoutInflater;
    }

    @Override
    public Context getHostContext() {
        return this.context;
    }

    @Override
    public MapView.NavigationController getNavigationController() {
        if(mapFragment!=null){
            return mapFragment.getMapView().getNavigation();
        }
        return null;
    }

    // lifecycle like methods
    @Override
    public void setupView(final IMainActivityPresenterCallback presenter) {
        this.presenter = presenter;

        this.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handlePoiSave();
            }
        });

        this.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.searchClicked();
            }
        });

        this.presenter.viewSetUp();
    }

    private void handlePoiSave() {
        String desc = this.saveEditText.getText().toString();
        IndoorwayPosition indoorwayPosition = this.mapFragment.getLastKnownPosition();

        presenter.saveLocationClicked(desc, indoorwayPosition);
    }

    @Override
    public void configView() {
        this.tabLayout.removeAllTabs();

        //
        this.tabLayout.addTab(tabLayout.newTab().setText("Zajęcia"));
        this.tabLayout.addTab(tabLayout.newTab().setText("PoI"));
        this.tabLayout.addTab(tabLayout.newTab().setText("Znajomi"));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            FontTextView tv=(FontTextView) inflater.inflate(R.layout.custom_tab_layout,null);
            tabLayout.getTabAt(i).setCustomView(tv);

        }

        this.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                int colorFrom = ((ColorDrawable) tabLayout.getBackground()).getColor();
//                int colorTo = getColorForTab(tabLayout.getSelectedTabPosition());
//
//                ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
//                colorAnimation.setDuration(1000);
//                colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                    @Override
//                    public void onAnimationUpdate(ValueAnimator animator) {
//                        Log.d("myTag", "onAnimationUpdate: animator = "+animator.getAnimatedValue());
//                        int color = (int) animator.getAnimatedValue();
//
////                        toolbar.setBackgroundColor(color);
//                        tabLayout.setBackgroundColor(color);
////                        floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(color));
//
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            ((MainActivity)context).getWindow().setStatusBarColor(color);
//                        }
//
//                    }
//
//                });
//
//                colorAnimation.start();

                presenter.tabSelected(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                clearView();
                clearMap();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        presenter.tabLayoutConfigured();
    }

    private void clearView(){
        this.detailsContainer.setVisibility(View.INVISIBLE);
        this.firstDetailsContainer.setVisibility(View.INVISIBLE);
        this.saveContainer.setVisibility(View.GONE);
        this.searchButton.setVisibility(View.GONE);

    }

    @Override
    public void onPause() {

        if(initialLocationListener!=null){
            IndoorwayLocationSdk.instance()
                    .state()
                    .onChange()
                    .unregister(initialLocationListener);
        }

        if(eventsListener!=null){
            IndoorwayLocationSdk.instance()
                    .customProximityEvents()
                    .onEvent()
                    .unregister(eventsListener);
        }

    }

    @Override
    public void setMapFragment(MapFragment mapFragment) {
        this.mapFragment = mapFragment;

        configMapView();
    }

    private void configMapView() {

        // on map loaded listener
        mapFragment.getMapView()
                // optional: assign callback for map loaded event
                .setOnMapLoadCompletedListener(new Action1<IndoorwayMap>() {
                    @Override
                    public void onAction(IndoorwayMap indoorwayMap) {
                        // called on every new map load success

                        configMarkersLayer();

                        presenter.mapLoaded();
                    }
                });

        // start positioning
        mapFragment.startPositioningService();

        // load building and floor
        mapFragment.getMapView()
                // perform map loading using building UUID and map UUID
                .load(miniId, floor2Id);

        // wait for first location - init and register listener
        this.initialLocationListener = new Action1<IndoorwayLocationSdkState>() {
            @Override
            public void onAction(IndoorwayLocationSdkState indoorwayLocationSdkState) {
                Log.d(TAG, "onAction: location = "+indoorwayLocationSdkState.toString());
                if(indoorwayLocationSdkState.toString().equals("LOCATING_FOREGROUND")){
                    presenter.locationObtained();
                    if(mapFragment!=null && mapFragment.getLastKnownPosition()!=null){
                        mapFragment.getMapView().getCamera().setPosition(mapFragment.getLastKnownPosition().getCoordinates());
                        mapFragment.getMapView().getCamera().setScale(1f, ScaleFactorType.METRIC);
                    }
                    initialLocationListener = null;
                }
                // handle state changes
            }
        };

        IndoorwayLocationSdk.instance()
                .state()
                .onChange()
                .register(initialLocationListener);
    }

    private void configMarkersLayer() {
        visitorCounter = 0;
        poiConuter = 0;

        markersLayer = mapFragment.getMapView().getMarker().addLayer(5);

        Bitmap breakBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.resized006);
        markersLayer.registerTexture(new BitmapTexture(context.getString(R.string.break_texture_id), breakBmp));

        Bitmap wcBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.resized007);
        markersLayer.registerTexture(new BitmapTexture(context.getString(R.string.toilet_texture_id), wcBmp));

        Bitmap printerBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.resized004);
        markersLayer.registerTexture(new BitmapTexture(context.getString(R.string.printer_texture_id), printerBmp));

        Bitmap deanBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.resized005);
        markersLayer.registerTexture(new BitmapTexture(context.getString(R.string.dean_texture_id), deanBmp));

        Bitmap feetBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.footsteps_black);
        markersLayer.registerTexture(new BitmapTexture(context.getString(R.string.feet_texture_id), feetBmp));
    }



    private void clearMap() {
        Log.d(TAG, "clearMap: visitorCounter = "+visitorCounter+", poiConuter = "+ poiConuter);
        for(int i=0;i<visitorCounter;i++){

            markersLayer.remove("visitor"+i);
            markersLayer.remove("visitor_txt"+i);
        }
        visitorCounter = 0;

        for(int i = 0; i< poiConuter; i++){
            String id = "roi"+i;
            markersLayer.remove(id);
        }

        poiConuter = 0;

        mapFragment.getMapView().getNavigation().stop();
        IndoorwayLocationSdk.instance().customProximityEvents().remove("navigation-target");
    }

    // next class module
    @Override
    public void showGoHome() {
        this.firstDetailsTextView.setText(R.string.no_classes_text);
        this.firstDetailsContainer.setVisibility(View.VISIBLE);
        this.detailsContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNextClassInfo(NextLessonEntity item) {
        Log.d(TAG, "showNextClassInfo: name = "+item.getName()+", room = "+item.getRoom()+", time = "+item.getTime());
        this.firstDetailsContainer.setVisibility(View.VISIBLE);

        this.firstDetailsTextView.setText(item.getName()+" w "+item.getRoom()+" o "+item.getTime());

        this.detailsContainer.setVisibility(View.VISIBLE);
    }

    // roi module
    @Override
    public void showRois(ArrayList<PoiEntity> poiList) {
        this.searchButton.setVisibility(View.VISIBLE);
        for(int i=0;i<poiList.size();i++){
            PoiEntity poiEntity = poiList.get(i);

            String bitmapTextureId;

            // there must be hard typed value
            switch (poiEntity.getType()){
                case "Odpoczynek":
                    bitmapTextureId = "bitmap_break";
                    break;
                case "Toaleta":
                    bitmapTextureId = "bitmap_wc";
                    break;
                case "Drukarka":
                    bitmapTextureId = "bitmap_printer";
                    break;
                case "Dziekanat":
                    bitmapTextureId = "bitmap_dean";
                    break;

                default: bitmapTextureId = null;
            }

            if(bitmapTextureId!=null){
                String id = "roi"+ poiConuter;
                Log.d("markerTag", "showRois: add = "+id);
                markersLayer.add(
                        new DrawableIcon(
                                id,    // icon identifier
                                bitmapTextureId, // texture identifier
                                poiEntity.getCoordinates(),
                                1.5f,  // icon size vertically
                                1.5f   // icon size horizontally
                        )
                );
                poiConuter++;
            }
        }
    }

    @Override
    public void showTypeChooseDialog(ArrayList<String> types) {
        new ChooseTypeDialog(context, inflater, types, new ChooseTypeDialog.IChooseTypeDialogCallback() {
            @Override
            public void typeChosen(String type) {
                presenter.typeChosen(type);
            }
        }).show();
    }



    // friends module
    @Override
    public void showVisitors(ArrayList<VisitorLocation> visitorLocations) {
        Log.d(TAG, "showVisitors: visitors = "+visitorLocations.size());
        for(VisitorLocation visitorLocation: visitorLocations){
            Log.d(TAG, "showVisitors: visitor = "+visitorLocation.getVisitorUuid());
            RegisteredVisitor visitor = presenter.findVisitor(visitorLocation.getVisitorUuid());
            if(visitor!=null) {
                Log.d(TAG, "showVisitors: visitor = " + visitor.getName());
                markersLayer.add(
                        new DrawableIcon(
                                "visitor"+visitorCounter,    // icon identifier
                                "bitmap_feet", // texture identifier
                                visitorLocation.getPosition().getCoordinates(),
                                2f,  // icon size vertically
                                2f   // icon size horizontally
                        )
                );

                markersLayer.add(
                        new DrawableText(
                                "visitor_txt"+visitorCounter,
                                visitorLocation.getPosition().getCoordinates(),
                                visitor.getName(),
                                1f	// eg. 2f
                        )
                );

                visitorCounter++;
            }
        }
    }

    //navigation module
    @Override
    public void startNavigation(Coordinates coordinates) {
        Log.d(TAG, "startNavigation: ");
        if(mapFragment!=null){
            addNavigationEvent(coordinates);

            mapFragment.getMapView().getNavigation().start(mapFragment.getLastKnownPosition(),
                    coordinates);
        }
    }

    private void addNavigationEvent(Coordinates coordinates) {
        Log.d(TAG, "addNavigationEvent: ");
        IndoorwayLocationSdk.instance().customProximityEvents()
                .add(new IndoorwayProximityEvent(
                        "navigation-target", // identifier
                        IndoorwayProximityEvent.Trigger.ENTER, // trigger on enter or on exit?
                        new IndoorwayProximityEventShape.Circle(
                                coordinates,
                                3.0
                        ),
                        miniId, // building identifier
                        floor2Id, // map identifier
                        1000L, // (optional) timeout to show notification, will be passed as parapeter to listener
                        new IndoorwayNotificationInfo("Pokoj", "Udalo Ci się dotrzeć, powodzenia!", null, null) // (optional) data to show in notification
                ));

        this.eventsListener = new Action1<IndoorwayProximityEvent>() {
            @Override
            public void onAction(IndoorwayProximityEvent indoorwayProximityEvent) {
                Log.d(TAG, "onAction: ");
                // handle proximity event
                if(indoorwayProximityEvent.getIdentifier().equals("navigation-target")){
                    // show navigation info dialog
                    mapFragment.getMapView().getNavigation().stop();
                    navigationLocationListener = null;
                    IndoorwayLocationSdk.instance().customProximityEvents().remove("navigation-target");
                    Log.d(TAG, "onAction: right target");
                }
            }
        };


        IndoorwayLocationSdk.instance()
                .customProximityEvents()
                .onEvent()
                .register(eventsListener);
    }

    // error dialog
    @Override
    public void showNetworkErrorDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(R.string.error_dialog_title)
                .setMessage(R.string.error_dialog_description)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // only if server fails
                    }
                })
                .show();
    }

    // save - normally disabled
    @Override
    public void showSaveLayout() {
        this.saveContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSuccessToast() {
        Toast.makeText(context, "Roi added!",
                Toast.LENGTH_LONG).show();
    }
}
