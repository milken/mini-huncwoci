package pro.huncwoci.minihuncwoci.activities.main;

import android.content.Context;

import com.indoorway.android.common.sdk.model.Coordinates;
import com.indoorway.android.common.sdk.model.VisitorLocation;
import com.indoorway.android.fragments.sdk.map.MapFragment;
import com.indoorway.android.map.sdk.view.MapView;

import java.util.ArrayList;

import pro.huncwoci.minihuncwoci.models.next_lesson.NextLessonEntity;
import pro.huncwoci.minihuncwoci.models.pois.PoiEntity;

/**
 * Created by su on 13.01.18.
 */

public interface IMainActivityViewCallback {

    // get view references
    Context getHostContext();
    MapView.NavigationController getNavigationController();

    // lifecycle like methods
    void setupView(IMainActivityPresenterCallback presenter);
    void configView();
    void onPause();
    void setMapFragment(MapFragment mapFragment);

    // next class module
    void showGoHome();
    void showNextClassInfo(NextLessonEntity item);

    // roi module
    void showRois(ArrayList<PoiEntity> poiList);
    void showTypeChooseDialog(ArrayList<String> types);

    // friends module
    void showVisitors(ArrayList<VisitorLocation> visitors);

    // navigation to cords
    void startNavigation(Coordinates coordinates);

    // network error
    void showNetworkErrorDialog();

    // save - normally disabled
    void showSaveLayout();
    void showSuccessToast();

}