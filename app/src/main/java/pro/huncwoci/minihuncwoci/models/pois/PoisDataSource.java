package pro.huncwoci.minihuncwoci.models.pois;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.DataSource;
import pro.huncwoci.minihuncwoci.models.base.Entity;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;

/**
 * Created by su on 13.01.18.
 */

public class PoisDataSource extends DataSource {

    public PoisDataSource() {
        super("/pois?hash="+ SessionManager.getUniqueKey());
    }

    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new PoisEntity(jsonObject);
    }
}
