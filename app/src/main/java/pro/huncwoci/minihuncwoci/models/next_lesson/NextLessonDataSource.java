package pro.huncwoci.minihuncwoci.models.next_lesson;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.DataSource;
import pro.huncwoci.minihuncwoci.models.base.Entity;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;

/**
 * Created by su on 13.01.18.
 */

public class NextLessonDataSource extends DataSource {

    public NextLessonDataSource() {
        super("/classes?hash="+ SessionManager.getUniqueKey());
    }

    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new NextLessonEntity(jsonObject);
    }
}
