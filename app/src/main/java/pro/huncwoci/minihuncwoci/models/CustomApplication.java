package pro.huncwoci.minihuncwoci.models;

/**
 * Created by su on 13.01.18.
 */

import android.app.Application;

import com.indoorway.android.common.sdk.IndoorwaySdk;

/**
 * Application's class. Defined in AndroidManifest.xml
 * Initializes Indoorway sdk modules.
 */
public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // init application context on each Application start
        IndoorwaySdk.initContext(this);

        // it's up to you when to initialize IndoorwaySdk, once initialized it will work forever!
        IndoorwaySdk.configure("6cccb9dd-1247-47e5-9504-767a9030a537");
    }

}
