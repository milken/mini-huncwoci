package pro.huncwoci.minihuncwoci.models.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by su on 13.01.18.
 */

public class PhotoUploadRequest extends StringRequest {

    public interface IPhotoUploadCallback{
        void photoUploaded(boolean success, long photoId, String photoUrl);
    }

    private static final String FILE_PART_NAME = "file";

    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private final byte[] bitmapByteArray;

    public PhotoUploadRequest(String url, byte[] bitmapByteArray, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, listener, errorListener);

        Log.d("myTag", "REQUEST URL = "+url);

        this.bitmapByteArray = bitmapByteArray;

        buildMultipartEntity();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if(headers == null || headers.equals(Collections.<String, String>emptyMap())) {
            headers = new HashMap<>();
        }

        headers.put("Authorization", SessionManager.getAccessToken());
        headers.put("accept", "application/json");

        Log.d("myTag", "HEADERS = "+headers.toString());

        return headers;
    }

    private void buildMultipartEntity() {
        Log.d("myTag", "FILE SIZE = "+bitmapByteArray.length);
        mBuilder.addBinaryBody(FILE_PART_NAME, bitmapByteArray, ContentType.create("image/png"), "file");
        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
    }

    @Override
    public String getBodyContentType() {
        String contentTypeHeader = mBuilder.build().getContentType().getValue();
        return contentTypeHeader;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try
        {
            mBuilder.build().writeTo(bos);
            Log.d("myTag", "bos = "+bos);
        }
        catch (IOException e)
        {
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }

        return bos.toByteArray();
    }

}

