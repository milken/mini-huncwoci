package pro.huncwoci.minihuncwoci.models.login;

import com.android.volley.Request;

import java.util.HashMap;
import java.util.Map;

import pro.huncwoci.minihuncwoci.models.base.ActionManager;
import pro.huncwoci.minihuncwoci.models.base.SessionManager;

/**
 * Created by su on 13.01.18.
 */

public class LoginActionManager {

    public static void logIn(ActionManager.IActionCallback callback){
        ActionManager.getInstance().performAction(Request.Method.GET, "/login?hash="+ SessionManager.getUniqueKey(), callback, null);
    }
}
