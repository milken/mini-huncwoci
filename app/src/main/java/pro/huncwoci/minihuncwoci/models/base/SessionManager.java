package pro.huncwoci.minihuncwoci.models.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by su on 13.01.18.
 */

public class SessionManager {

//    public static final String BASE_URL = "http://dc4f9d8a.ngrok.io";
    public static final String BASE_URL = "http://138.68.66.216:10000";

    //
    public static SessionManager instance = null;

    public static String userName = "";

    //
    private static SharedPreferences sharedPreferences;
    private RequestQueue requestQueue;

    private static String ACCESS_TOKEN = "";
    private static boolean isDataLoaded = false;

    //singleton
    public static SessionManager getInstance(Context context) {
        if(instance == null || sharedPreferences == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    private SessionManager(Context context){
        sharedPreferences = context.getSharedPreferences("HuncowciSharedPreferences", Context.MODE_PRIVATE);
        this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static String getAccessToken() {
        return sharedPreferences.getString("ACCESS_TOKEN", null);
    }

    public static void logIn(String accessToken) {
        Log.d("myTag","accessToken: "+accessToken);
        ACCESS_TOKEN = accessToken;
        sharedPreferences.edit().putString("ACCESS_TOKEN", ACCESS_TOKEN).apply();
    }

    public static boolean isLoggedIn() {
        ACCESS_TOKEN = sharedPreferences.getString("ACCESS_TOKEN", null);
        return ACCESS_TOKEN != null;
    }

    public static void logOut(){
        sharedPreferences.edit().remove("ACCESS_TOKEN").apply();
        ACCESS_TOKEN = null;
    }

    public static boolean hasUniqueKey() {
//        return getUniqueKey()!=null;
        return false;
    }

    public static boolean isVisitorSet(){
        return sharedPreferences.getBoolean("IS_VISITOR_SET", false);
    }

    public static void visitorSet(){
        sharedPreferences.edit().putBoolean("IS_VISITOR_SET", true).apply();

    }

    public static String getUniqueKey(){
        return sharedPreferences.getString("UNIQUE_KEY", null);
    }

    public static void setUniqueKey(String uniqueKey){
        sharedPreferences.edit().putString("UNIQUE_KEY", uniqueKey).apply();
    }
}
