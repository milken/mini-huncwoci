package pro.huncwoci.minihuncwoci.models.test;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class TestEntity extends Entity implements Parcelable {

    private String name;
    private ArrayList<Integer> genders = new ArrayList<>();

    public TestEntity() {
        super();
        name = "";
        this.genders = new ArrayList<>();
    }

    public TestEntity(JSONObject object) throws JSONException {
        super(object);
        try {this.name = object.getString("name");} catch (JSONException e) {e.printStackTrace();}

        try {
            JSONArray array = object.getJSONArray("genders");
            for(int i = 0; i<array.length(); i++) {
                this.genders.add(array.getInt(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected TestEntity(Parcel in) {
        super(in);
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TestEntity> CREATOR = new Creator<TestEntity>() {
        @Override
        public TestEntity createFromParcel(Parcel in) {
            return new TestEntity(in);
        }

        @Override
        public TestEntity[] newArray(int size) {
            return new TestEntity[size];
        }
    };

    /**
     * GETTERS
     */

    public String getName() {
        return name;
    }

    public ArrayList<Integer> getGenders() {
        return genders;
    }
}

