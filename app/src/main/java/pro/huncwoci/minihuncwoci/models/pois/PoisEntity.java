package pro.huncwoci.minihuncwoci.models.pois;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class PoisEntity extends Entity implements Parcelable{

    private ArrayList<PoiEntity> poiList = new ArrayList<>();
    private String name;

    public PoisEntity() {
    }

    public PoisEntity(JSONObject json) throws JSONException {
        Log.d("myTag", "PoisEntity: ");

        try {this.name = json.getJSONObject("personal_data").getString("name");} catch (JSONException e) {e.printStackTrace();}

        try {
            JSONArray array = json.getJSONArray("pois");
            Log.d("myTag", "PoisEntity: array length = "+array.length());
            for(int i = 0; i<array.length(); i++) {
                this.poiList.add(new PoiEntity(array.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected PoisEntity(Parcel in) {
        super(in);
        poiList = in.createTypedArrayList(PoiEntity.CREATOR);
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(poiList);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PoisEntity> CREATOR = new Creator<PoisEntity>() {
        @Override
        public PoisEntity createFromParcel(Parcel in) {
            return new PoisEntity(in);
        }

        @Override
        public PoisEntity[] newArray(int size) {
            return new PoisEntity[size];
        }
    };

    public ArrayList<PoiEntity> getPoiList() {
        return poiList;
    }

    public String getName() {
        return name;
    }
}
