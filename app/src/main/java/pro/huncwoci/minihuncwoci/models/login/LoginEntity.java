package pro.huncwoci.minihuncwoci.models.login;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class LoginEntity extends Entity {

    private String url;
    private String token;

    public LoginEntity() {
    }

    public LoginEntity(JSONObject json) throws JSONException {
        try {this.url = json.getString("url");} catch (JSONException e) {e.printStackTrace();}
        try {this.token = json.getString("token");} catch (JSONException e) {e.printStackTrace();}
    }

    public String getUrl() {
        return url;
    }

    public String getToken() {
        return token;
    }
}
