package pro.huncwoci.minihuncwoci.models.base;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by su on 13.01.18.
 */

public class Entity implements Parcelable {

    protected int id;

    public Entity(JSONObject json) throws JSONException {
        // map values
        this.id = json.getInt("id");

    }

    public Entity() {
        this(0);
    }

    public Entity(int id) {
        this.id = id;
    }


    //

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    // Parcelable

    protected Entity(Parcel in) {
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
        @Override
        public Entity createFromParcel(Parcel in) {
            return new Entity(in);
        }

        @Override
        public Entity[] newArray(int size) {
            return new Entity[size];
        }
    };
}
