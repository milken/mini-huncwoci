package pro.huncwoci.minihuncwoci.models.test;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.DataSource;
import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class TestDataSource extends DataSource {

    public TestDataSource() {
        super("/api/cos/");
    }

    @Override
    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new TestEntity(jsonObject);
    }
}
