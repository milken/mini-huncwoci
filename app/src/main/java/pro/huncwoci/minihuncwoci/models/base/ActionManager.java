package pro.huncwoci.minihuncwoci.models.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by su on 13.01.18.
 */

public class ActionManager {

    private RequestQueue requestQueue;
    private static ActionManager instance = null;

    public static ActionManager getInstance() {
        if(instance == null) {
            instance = new ActionManager();
        }

        return instance;
    }

    public interface IActionCallback {
        void completed(boolean success, int errorCode, JSONObject response);
    }

    public ActionManager() {
        this.requestQueue = SessionManager.instance.getRequestQueue();
    }


    //
    public void performAction(int method, String url, final IActionCallback callback, final Map<String, String> params) {

        final String mobileUrl = SessionManager.BASE_URL + url;

        Log.d("myTag", "NEW ACTION = " + mobileUrl);
        Log.d("myTag","method = "+method);
        final StringRequest request = new StringRequest(method, mobileUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("myTag", "ACTION RESPONSE = " + response);
                JSONObject jsonResponse = null;
                try {
                    jsonResponse = new JSONObject(response);
                    if(jsonResponse.getBoolean("success") == true) {
                        callback.completed(true, 0, jsonResponse);
                    } else {
                        callback.completed(false, 4, jsonResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.completed(false, 4, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("myTag", "ERROR = " + String.valueOf(error));
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;
                try {
                    //for getting error text from error
                    if(error.getMessage() != null) {
                        JSONObject object = new JSONObject(error.getMessage());
                        object.put("error_text", object.getString("detail"));
                        callback.completed(false, statusCode, object);
                    }else {
                        callback.completed(false, statusCode, null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.completed(false, statusCode, null);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // authorization
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", SessionManager.getAccessToken());
                Log.d("myTag", "TOKEN = "+SessionManager.getAccessToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {

                //for getting texts from volley errors -_-
                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }

                return volleyError;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        this.requestQueue.add(request);
    }

    public String trimMessage(String json, String key){
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }


    public void performJsonAction(String url, final IActionCallback callback, final Map<String, Object> params) {

        final String mobileUrl = SessionManager.BASE_URL + url;

        Log.d("myTag", "NEW ACTION = " + mobileUrl);
        final JSONObject jsonObject = new JSONObject(params);
        Log.d("myTag", "JSON OBJECT = "+jsonObject.toString());

        JsonObjectRequest jsonRequest = new JsonObjectRequest(mobileUrl, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("myTag", "response = " + response);
                JSONObject jsonResponse = null;
                try {
                    jsonResponse = response;
                    if(jsonResponse.getBoolean("success") == true) {
                        callback.completed(true, 0, jsonResponse);
                    } else {
                        callback.completed(false, 4, jsonResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.completed(false, 4, null);
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("myTag", "ERROR = " + String.valueOf(error));
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;

                callback.completed(false, statusCode, null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // authoriation
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", SessionManager.getAccessToken());
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            protected String getParamsEncoding() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return jsonObject.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        this.requestQueue.add(jsonRequest);
    }
}

