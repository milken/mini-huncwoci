package pro.huncwoci.minihuncwoci.models.base;

import com.indoorway.android.common.sdk.model.Coordinates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by su on 13.01.18.
 */

public class UserActionManager {

    public static void postPoi(String name, Coordinates coordinates, String floor, ActionManager.IActionCallback callback){

        Map<String, Object> params = new HashMap<>();

        params.put("name", name);
        params.put("lat", coordinates.getLatitude());
        params.put("lng", coordinates.getLongitude());
        params.put("floor", floor);

        ActionManager.getInstance().performJsonAction("/pois", callback, params);
    }
}
