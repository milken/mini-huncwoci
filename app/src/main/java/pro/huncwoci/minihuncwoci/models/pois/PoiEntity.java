package pro.huncwoci.minihuncwoci.models.pois;

import android.os.Parcel;
import android.os.Parcelable;

import com.indoorway.android.common.sdk.model.Coordinates;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class PoiEntity extends Entity implements Parcelable {

    private String name;
    private double lat;
    private double lng;
    private int floorLevel;
    private String type;

    public PoiEntity() {
    }

    public PoiEntity(JSONObject object) throws JSONException {
        super(object);
        try {this.name = object.getString("name");} catch (JSONException e) {e.printStackTrace();}
        try {this.lat = object.getDouble("lat");} catch (JSONException e) {e.printStackTrace();}
        try {this.lng = object.getDouble("lng");} catch (JSONException e) {e.printStackTrace();}
        try {this.floorLevel = object.getInt("floor");} catch (JSONException e) {e.printStackTrace();}
        try {this.type = object.getString("type");} catch (JSONException e) {e.printStackTrace();}

    }

    protected PoiEntity(Parcel in) {
        super(in);
        name = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        floorLevel = in.readInt();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeInt(floorLevel);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PoiEntity> CREATOR = new Creator<PoiEntity>() {
        @Override
        public PoiEntity createFromParcel(Parcel in) {
            return new PoiEntity(in);
        }

        @Override
        public PoiEntity[] newArray(int size) {
            return new PoiEntity[size];
        }
    };

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public int getFloorLevel() {
        return floorLevel;
    }

    public String getType() {
        return type;
    }

    public Coordinates getCoordinates(){
        return new Coordinates(lat,lng);
    }
}
