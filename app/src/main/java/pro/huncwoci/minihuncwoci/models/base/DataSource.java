package pro.huncwoci.minihuncwoci.models.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pro.huncwoci.minihuncwoci.callbacks.IDataSourceCallback;

/**
 * Created by su on 13.01.18.
 */

public class DataSource {

    protected RequestQueue requestQueue;

    protected Request request;

    protected Entity item;

    private boolean isLoaded;

    private Map<String, String> params;

    private String url;

    public DataSource(String url) {
        this.requestQueue = SessionManager.instance.getRequestQueue();

        this.isLoaded = false;

        this.setUrl(url);
    }

    // TO BE OVERRIDDEN

    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Entity(jsonObject);
    }


    //

    public Entity getItem() {
        return this.item;
    }

    public void clearData() {
        this.item = null;
    }

    public void cancel() {
        if(this.request != null)
            this.request.cancel();
    }

    public void setUrl(String url) {
        this.url = SessionManager.BASE_URL + url;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }


    public boolean fetchData(final IDataSourceCallback callback)
    {
        this.cancel();

        Log.d("myTag", "NEW REQUEST = " + this.url);

        int method = Request.Method.GET;
        if(this.params != null)
            method = Request.Method.POST;

        this.request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("myTag", "REQUEST OK");
                Log.d("myTag", ""+response.toString());

                try {
                    item = makeEntity(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                callback.fetchCompleted(true, 0, 1);

                request = null;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("myTag", "REQUEST ERROR : " + error);
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;
                callback.fetchCompleted(false, statusCode, 0);

                request = null;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("AUTHTOKEN", SessionManager.getAccessToken());
                return headers;
            }
        };

        if(this.request == null) {
            callback.fetchCompleted(true, 0, 1);
        }
        else {
            this.requestQueue.add(this.request);
        }
        return true;
    }

}