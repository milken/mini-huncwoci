package pro.huncwoci.minihuncwoci.models.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pro.huncwoci.minihuncwoci.callbacks.IDataSourceCallback;

/**
 * Created by su on 13.01.18.
 */

public class ListDataSource {

    protected RequestQueue requestQueue;
    protected Request request;

    // data to be shown in list etc.
    protected ArrayList<Entity> itemsList;

    private boolean isAllLoaded;
    private boolean loadMore;

    private Map<String, String> params;

    protected String url;
    protected String nextUrl;

    //


    // TO BE OVERRIDDEN

    public Entity makeEntity(JSONObject jsonObject) throws JSONException {
        return new Entity(jsonObject);
    }


    //

    public ListDataSource(String url) {
        this.requestQueue = SessionManager.instance.getRequestQueue();
        this.params = null;
        this.isAllLoaded = false;
        this.itemsList = new ArrayList<>();
        this.setLoadMore(false);
        this.setUrl(url);
    }

    public boolean isLoadMore() {
        return loadMore;
    }

    public void setLoadMore(boolean loadMore) {
        this.loadMore = loadMore;
    }

    public ArrayList<Entity> getItemsList() {
        return itemsList;
    }

    public void setItemsList(ArrayList<Entity> itemsList) {
        this.itemsList = itemsList;
    }

    public Entity getItemAtPos(int pos) {
        return this.itemsList.get(pos);
    }

    public int getItemsCount() {
        return this.itemsList.size();
    }

    public void clearData() {
        this.itemsList.clear();
    }

    public void cancel() {
        if(this.request != null)
            this.request.cancel();
    }

    protected void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setUrl(String url) {
        this.url = SessionManager.BASE_URL + url;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public void fetch(IDataSourceCallback callback) {
        fetchData(this.url, callback, true);
    }

    public void fetchMore(IDataSourceCallback callback) {
        Log.d("myTag", "FETCH MORE NEXT URL = "+this.nextUrl);
        if(this.nextUrl != null) {
            fetchData(this.nextUrl, callback, false);
        }
    }

    public void fetchArray(IDataSourceCallback callback){
        fetchLocation(this.url, callback, true);
    }

    //to fetch jsonArray
    private void fetchLocation(final String url, final IDataSourceCallback callback, final boolean isInitLoading) {
        if(isInitLoading) {
            this.cancel();
            this.isAllLoaded = false;
        }
        // smth is being fetched
        else if(this.request != null || this.isAllLoaded) {
            // !better add fragmentCallback to queue to be notified when finished!
            Log.d("myTag", "DONT REQUEST " + this.request + ", isAllLoaded = " + isAllLoaded);
        }

        Log.d("myTag", "NEW REQUEST = " + this.url);

        int method = Request.Method.GET;
        if(this.params != null)
            method = Request.Method.POST;

        this.request = new JsonArrayRequest(method, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("myTag", "DONE NEW REQUEST = " +url);

                if (isInitLoading)
                    itemsList.clear();

                for(int i=0; i<response.length(); i++) {
                    try {
                        itemsList.add(makeEntity(response.getJSONObject(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (response.length() == 0)
                    isAllLoaded = true;

                callback.fetchCompleted(true, 0, response.length());

                request = null;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("myTag", "REQUEST ERROR " + error);
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;
                callback.fetchCompleted(false, statusCode, 0);

                // fetch completed
                request = null;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Log.d("myTag", "token = "+SessionManager.getAccessToken());
                headers.put("AUTHTOKEN", SessionManager.getAccessToken());
                return headers;
            }
        };

        if(this.request == null) {
            callback.fetchCompleted(true, 0, 1);
        }
        else {
            this.requestQueue.add(this.request);
        }
    }

    private boolean fetchData(final String url, final IDataSourceCallback callback, final boolean isInitLoading) {

        if(isInitLoading) {
            this.cancel();
            this.isAllLoaded = false;
        }

        // smth is being fetched
        else if(this.request != null || this.isAllLoaded) {
            // !better add fragmentCallback to queue to be notified when finished!
            Log.d("myTag", "DONT REQUEST " + this.request + ", isAllLoaded = " + isAllLoaded);
            return false;
        }

        Log.d("myTag", "NEW REQUEST = " + url);

        int method = Request.Method.GET;
        if(this.params != null){
            method = Request.Method.POST;
            Log.d("myTag","params: "+params.size());

            for(Map.Entry<String, String> entry: params.entrySet()){
                Log.d("myTag", "entry: "+entry.getKey()+" "+entry.getValue());
            }

        }

        Log.d("myTag", "METHOD = "+method);

        this.request = new JsonObjectRequest(method, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("myTag", "REQUEST OK");
                Log.d("myTag", "" + response.toString());

                //clear item list when we start to fetch data
                if (isInitLoading)
                    itemsList.clear();

                JSONArray jsonArray = null;
                JSONObject jsonObject = null;
                try {

                    jsonArray = response.getJSONArray("results");
                    nextUrl = response.getString("next");


                    Log.d("myTag", "nextUrl = "+nextUrl);

                    if(nextUrl == null) {
                        Log.d("myTag", "isAllLoaded");
                        isAllLoaded = true;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    //when next url exist "next" is jsonObject in other case "next" is empty String and we catch cast exception
                    isAllLoaded = true;
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        itemsList.add(makeEntity(jsonArray.getJSONObject(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (response.length() == 0)
                    isAllLoaded = true;

                callback.fetchCompleted(true, 0, response.length());

                request = null;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("myTag", "REQUEST ERROR " + error);
                int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 404;
                Log.d("myTag","statusCode: "+statusCode);

                callback.fetchCompleted(false, statusCode, 0);

                // fetch completed
                request = null;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Log.d("myTag", "TOKEN = "+SessionManager.getAccessToken());
                headers.put("AUTHTOKEN", SessionManager.getAccessToken());
                return headers;
            }
        };

        if(this.request == null) {
            callback.fetchCompleted(true, 0, 1);
        }
        else {
            this.requestQueue.add(this.request);
        }

        return true;
    }

}

