package pro.huncwoci.minihuncwoci.models.next_lesson;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.indoorway.android.common.sdk.model.Coordinates;

import org.json.JSONException;
import org.json.JSONObject;

import pro.huncwoci.minihuncwoci.models.base.Entity;

/**
 * Created by su on 13.01.18.
 */

public class NextLessonEntity extends Entity implements Parcelable {

    private String name;
    private int roomId;
    private String time;

    public NextLessonEntity() {
    }

    public NextLessonEntity(JSONObject json) throws JSONException {
//        super(json);
        Log.d("myTag", "NextLessonEntity: json = "+json);

        try {this.name = json.getString("name");} catch (JSONException e) {e.printStackTrace();}
        try {this.roomId = json.getInt("room");} catch (JSONException e) {e.printStackTrace();}
        try {this.time = json.getString("time");} catch (JSONException e) {e.printStackTrace();}


    }

    protected NextLessonEntity(Parcel in) {
        super(in);
        name = in.readString();
        roomId = in.readInt();
        time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(name);
        dest.writeInt(roomId);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NextLessonEntity> CREATOR = new Creator<NextLessonEntity>() {
        @Override
        public NextLessonEntity createFromParcel(Parcel in) {
            return new NextLessonEntity(in);
        }

        @Override
        public NextLessonEntity[] newArray(int size) {
            return new NextLessonEntity[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getRoom() {
        return roomId;
    }

    public String getTime() {
        return time;
    }

//    public double getLat() {
//        return lat;
//    }
//
//    public double getLng() {
//        return lng;
//    }
//
//    public Coordinates getCoordinates(){
//        return new Coordinates(lat, lng);
//    }
}
