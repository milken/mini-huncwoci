package pro.huncwoci.minihuncwoci.views.adapters;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created by su on 13.01.18.
 */

public class HeaderFooterRecyclerViewAdapter extends RecyclerView.Adapter {

    private final int HEADER_VIEW_TYPE = 100;
    private final int FOOTER_VIEW_TYPE = 200;
    private final int EXTRA_VIEW_VIEW_TYPE = 300;

    private LayoutInflater inflater;

    private boolean hasHeader;
    private boolean hasFooter;
    private boolean hasExtraView;

    public HeaderFooterRecyclerViewAdapter() {
        super();
        this.hasExtraView = false;
        this.hasHeader = true;
        this.hasFooter = true;
    }

    public void addHeader() {
        this.hasHeader = true;
    }

    public void removeHeader() {
        this.hasHeader = false;
    }

    public void addFooter() {
        this.hasFooter = true;
    }

    public void removeFooter() {
        this.hasFooter = false;
    }

    public void addExtraCell() {
        this.hasExtraView = true;
    }

    public void removeExtraCell() {
        this.hasExtraView = false;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setAsGrid(final GridLayoutManager gridLayoutManager) {
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if ((hasHeader == true && position == 0) || (hasFooter == true && position == getItemCount() - 1)) {
                    return gridLayoutManager.getSpanCount();
                }
                return 1;
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == HEADER_VIEW_TYPE) {
            return onCreateHeaderViewHolder_(viewGroup);
        } else if (viewType == FOOTER_VIEW_TYPE) {
            return onCreateFooterViewHolder_(viewGroup);
        } else if (viewType == EXTRA_VIEW_VIEW_TYPE) {
            return onCreateExtraViewViewHolder_(viewGroup);
        }
        return onCreateViewHolder_(viewGroup, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int pos) {
        int type = getItemViewType(pos);
        if (type == HEADER_VIEW_TYPE) {
            onBindHeaderViewHolder_(viewHolder);
        } else if (type == FOOTER_VIEW_TYPE) {
            onBindFooterViewHolder_(viewHolder);
        } else if (type == EXTRA_VIEW_VIEW_TYPE) {
            onBindExtraViewViewHolder_(viewHolder);
        } else {
            onBindViewHolder_(viewHolder, pos - (this.hasExtraView == true ? 1 : 0) - (this.hasHeader == true ? 1 : 0));
        }
    }

    @Override
    public int getItemCount() {
        return getItemCount_() + (this.hasExtraView == true ? 1 : 0) + (this.hasHeader == true ? 1 : 0) + (this.hasFooter == true ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (this.hasHeader == true && position == 0) {
            return HEADER_VIEW_TYPE;
        } else if (this.hasFooter == true && position == getItemCount() - 1) {
            return FOOTER_VIEW_TYPE;
        } else if (position == (hasHeader ? 1 : 0) && this.hasExtraView == true) {
            return EXTRA_VIEW_VIEW_TYPE;
        }

        return getItemViewType_(position - (this.hasExtraView == true ? 1 : 0) - (this.hasHeader == true ? 1 : 0));
    }


    /////

    public int getItemCount_() {
        return 0;
    }

    public RecyclerView.ViewHolder onCreateViewHolder_(ViewGroup viewGroup, int i) {
        return null;
    }

    public void onBindViewHolder_(RecyclerView.ViewHolder viewHolder, int i) {

    }

    public RecyclerView.ViewHolder onCreateHeaderViewHolder_(ViewGroup viewGroup) {
        return null;
    }

    public RecyclerView.ViewHolder onCreateFooterViewHolder_(ViewGroup viewGroup) {
        return null;
    }

    public int getItemViewType_(int position) {
        return 0;
    }

    public void onBindHeaderViewHolder_(RecyclerView.ViewHolder viewHolder) {
    }

    public void onBindFooterViewHolder_(RecyclerView.ViewHolder viewHolder) {
    }

    public RecyclerView.ViewHolder onCreateExtraViewViewHolder_(ViewGroup viewGroup) {
        return null;
    }

    public void onBindExtraViewViewHolder_(RecyclerView.ViewHolder viewHolder) {
    }
}