package pro.huncwoci.minihuncwoci.views.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by su on 13.01.18.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {

    protected View itemView_;

    public BaseViewHolder(View itemView_) {
        super(itemView_);
        this.itemView_ = itemView_;
    }

}
