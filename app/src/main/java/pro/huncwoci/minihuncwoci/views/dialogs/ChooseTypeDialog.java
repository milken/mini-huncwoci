package pro.huncwoci.minihuncwoci.views.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.callbacks.IItemClickCallback;
import pro.huncwoci.minihuncwoci.views.viewholders.DialogItemViewHolder;

/**
 * Created by su on 13.01.18.
 */

public class ChooseTypeDialog extends Dialog {

    public interface IChooseTypeDialogCallback{
        void typeChosen(String type);
    }

    @BindView(R.id.choose_type_dialogRecyclerView)
    RecyclerView recyclerView;

    public ChooseTypeDialog(@NonNull Context context, LayoutInflater inflater, ArrayList<String> types, IChooseTypeDialogCallback callback) {
        super(context, R.style.fadeDialog);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.choose_type_dialog);
        ButterKnife.bind(this);

        configView(inflater, types, callback);
    }

    private void configView(final LayoutInflater inflater, final ArrayList<String> types, final IChooseTypeDialogCallback callback) {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.recyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new DialogItemViewHolder(inflater.inflate(R.layout.dialog_item_view_holder, parent, false));
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                ((DialogItemViewHolder) holder).configure(position, types.get(position), new IItemClickCallback() {
                    @Override
                    public void itemClicked(int position) {
                        callback.typeChosen(types.get(position));
                        dismiss();
                    }
                });
            }

            @Override
            public int getItemCount() {
                return types.size();
            }
        });
    }
}
