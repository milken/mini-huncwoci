package pro.huncwoci.minihuncwoci.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;

import pro.huncwoci.minihuncwoci.R;

/**
 * Created by su on 13.01.18.
 */

public class LoadingDialog extends Dialog {

    public LoadingDialog(@NonNull Context context, boolean transparent) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //clear dim behind dialog
        if(transparent) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        setContentView(R.layout.loading_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LoadingDialog.this.setCancelable(false);
        LoadingDialog.this.setCanceledOnTouchOutside(false);
    }
}

