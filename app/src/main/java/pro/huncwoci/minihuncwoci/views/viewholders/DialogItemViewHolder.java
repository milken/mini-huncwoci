package pro.huncwoci.minihuncwoci.views.viewholders;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.callbacks.IItemClickCallback;

/**
 * Created by su on 13.01.18.
 */

public class DialogItemViewHolder extends BaseViewHolder {

    @BindView(R.id.dialog_item_view_holderTextview)
    TextView textView;

    public DialogItemViewHolder(View itemView_) {
        super(itemView_);
        ButterKnife.bind(this, itemView_);
    }

    public void configure(final int position, String s, final IItemClickCallback iItemClickCallback) {
        textView.setText(s);
        itemView_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemClickCallback.itemClicked(position);
            }
        });
    }
}
