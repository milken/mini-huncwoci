package pro.huncwoci.minihuncwoci.views.base;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pro.huncwoci.minihuncwoci.R;

/**
 * Created by su on 13.01.18.
 */

public class CustomSearchView extends RelativeLayout {

    public interface ICustomSearchViewCallback{

        void backClicked();
        void searchClicked();
        void cancelClicked();
        void textChanged(String text);


    }

    private ImageView backIcon;
    private ImageView searchIcon;
    private ImageView cancelIcon;

    private EditText editText;

    private ICustomSearchViewCallback searchViewCallback;

    public CustomSearchView(Context context) {
        super(context);
        init(context);
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setSearchViewCallback(ICustomSearchViewCallback callback) {
        this.searchViewCallback = callback;
    }

    private void init(final Context context) {

        inflate(getContext(), R.layout.custom_search_view, this);

        this.editText = (EditText) findViewById(R.id.search_view_editText);
        this.backIcon = (ImageView) findViewById(R.id.search_view_backIcon);
        this.searchIcon = (ImageView) findViewById(R.id.search_view_searchIcon);
        this.cancelIcon = (ImageView) findViewById(R.id.search_view_cancelIcon);
        this.cancelIcon.setVisibility(GONE);

        this.backIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                searchViewCallback.backClicked();
            }
        });


        this.searchIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                searchViewCallback.searchClicked();
                searchIcon.setVisibility(GONE);
                cancelIcon.setVisibility(VISIBLE);

                editText.requestFocus();

                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);


            }
        });

        this.cancelIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                clearClicked(context);
            }
        });

        this.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchViewCallback.textChanged(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void clearClicked(Context context) {
        searchViewCallback.cancelClicked();
        cancelIcon.setVisibility(GONE);
        searchIcon.setVisibility(VISIBLE);

        editText.clearFocus();
        editText.clearComposingText();
        editText.setText("");
        searchViewCallback.textChanged("");
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}
