package pro.huncwoci.minihuncwoci.views.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import pro.huncwoci.minihuncwoci.R;
import pro.huncwoci.minihuncwoci.views.adapters.HeaderFooterRecyclerViewAdapter;

/**
 * Created by su on 13.01.18.
 */

public class LoadingRecyclerView extends LinearLayout {

    //

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView textView;

    //

    private String emptyText = "";

    //

    private boolean isLoading = false;

    //

    public LoadingRecyclerView(Context context) {
        super(context);

        init();
    }

    public LoadingRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public LoadingRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    //

    public void loading() {
        this.isLoading = true;
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
    }

    public void loaded() {
        this.isLoading = false;
    }

    public void updateData() {
        loaded();
        if(!this.isLoading) {
            progressBar.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);

            if (getAdapter() != null) {
                getAdapter().notifyDataSetChanged();

                // if header/footer we want only pure items
                if(getAdapter() instanceof HeaderFooterRecyclerViewAdapter) {
                    HeaderFooterRecyclerViewAdapter adapter = ((HeaderFooterRecyclerViewAdapter)getAdapter());
                    if(adapter.getItemCount_() == 0) {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(emptyText);
                    }
                } else {
                    if (getAdapter().getItemCount() == 0) {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(emptyText);
                    }
                }
            }
        }
    }

    public void setEmptyText(String emptyText) {
        this.emptyText = emptyText;
    }

    //

    private void init() {
        inflate(getContext(), R.layout.loading_recycler_view, this);

        this.recyclerView = (RecyclerView)findViewById(R.id.loading_recycler_view_recyclerView);
        this.progressBar = (ProgressBar)findViewById(R.id.loading_recycler_view_progressBar);
        this.textView = (TextView)findViewById(R.id.loading_recycler_view_infoTextView);

        // pass padding to RecyclerView
        this.recyclerView.setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        this.recyclerView.setClipToPadding(false);
        this.setPadding(0, 0, 0, 0);

    }

    // Methods as in RecyclerView

    public void setAdapter(RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    public void setLayoutManager(RecyclerView.LayoutManager layout) {
        recyclerView.setLayoutManager(layout);
    }

    public RecyclerView.Adapter getAdapter() {
        return recyclerView.getAdapter();
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return recyclerView.getLayoutManager();
    }

}

