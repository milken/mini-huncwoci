package pro.huncwoci.minihuncwoci.callbacks;

import android.app.Fragment;
import android.content.Intent;

/**
 * Created by su on 13.01.18.
 */

public interface IFragmentCallback {
    void showWindow(Fragment fragment, boolean isBack);

    //show fragment for result
    void showWindow(Fragment fragment, boolean isBack, Fragment targetFragment, int requestCode);
    void showToolbar();
    void hideToolbar();
    void showArrowBack();
    void hideArrowBack();
    void setTitle(String title);

    void backWindow();
    void backWindowWithResult(Fragment currentFragment, Intent data);
}
