package pro.huncwoci.minihuncwoci.callbacks;

/**
 * Created by su on 13.01.18.
 */

public interface IItemClickCallback {
    void itemClicked(int position);
}
