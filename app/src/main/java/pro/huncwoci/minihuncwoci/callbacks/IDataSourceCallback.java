package pro.huncwoci.minihuncwoci.callbacks;

/**
 * Created by su on 13.01.18.
 */

public interface IDataSourceCallback {
    void fetchCompleted(boolean success, int errorCode, int itemsCount);
}

