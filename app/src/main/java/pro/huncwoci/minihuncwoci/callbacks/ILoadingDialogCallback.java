package pro.huncwoci.minihuncwoci.callbacks;

/**
 * Created by su on 13.01.18.
 */

public interface ILoadingDialogCallback {

    void showLoadingDialog(boolean isTransparent);
    void hideLoadingDialog();

}
